import { observable, action, computed } from "mobx";
import { UserManager } from "oidc-client";

export class AuthenticationStore {
  @observable manager = null;
  @observable user = null;

  constructor() {
    let config = {
      authority:
        "https://cognito-idp.us-east-1.amazonaws.com/us-east-1_Nl7rkrtXL/",
      client_id: "75t37nlsbrgufsekjrkk7g8e93",
      response_type: "code",
      scope: "openid profile email aws.cognito.signin.user.admin",
      filterProtocolClaims: true,
      loadUserInfo: true,
      automaticSilentRenew: true,
      includeIdTokenInSilentRenew: true,
      revokeAccessTokenOnSignout: true,
      validateAccessTokenFromServer: true,
      create_user: true,
      api_link: "https://j0qjo4kjya.execute-api.us-east-2.amazonaws.com/dev",
      api_token: "456787-ecbc-4754-9490-5a5c5a72e04b",
      pool_id: "us-east-1_Nl7rkrtXL",
      is_username_equals_email: false,
      redirect_uri:
        window.location.protocol +
        "//" +
        window.location.host +
        "/" +
        "auth-callback",
      post_logout_redirect_uri:
        window.location.protocol + "//" + window.location.host,
      silent_redirect_uri:
        window.location.protocol +
        "//" +
        window.location.host +
        "/silent-refresh.html",
    };
    this.manager = new UserManager(config);
  }

  @computed
  get isLoggedIn() {
    return this.user != null && this.user.access_token && !this.user.expired;
  }

  @action.bound
  loadUser() {
    this.manager.getUser().then((user) => (this.user = user));
  }

  @action.bound
  login() {
    this.manager.signinRedirect().catch((error) => this.handleError(error));
  }

  @action.bound
  completeLogin() {
    this.manager
      .signinRedirectCallback()
      .then((user) => {
        this.user = user;
        console.log("hello");
      })
      .catch((error) => this.handleError(error));
  }

  @action.bound
  logout() {
    this.manager.signoutRedirect().catch((error) => this.handleError(error));
  }

  @action.bound
  completeLogout() {
    this.manager
      .signoutRedirectCallback()
      .then(() => {
        this.manager.removeUser();
      })
      .then(() => {
        this.user = null;
      })
      .catch((error) => this.handleError(error));
  }

  @action.bound
  handleError(error) {
    console.error("Problem with authentication endpoint: ", error);
  }
}
