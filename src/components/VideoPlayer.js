import React from "react";
import ReactPlayer from "react-player";

export default class VideoPlayer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      url: props.url,
    };
    this.onProgress = this.onProgress.bind(this);
    this.onDuration = this.onDuration.bind(this);
    this.onPlay = this.onPlay.bind(this);
    this.onPause = this.onPause.bind(this);
    this.onEnded = this.onEnded.bind(this);
  }

  onProgress(state) {
    this.setState(state);
  }

  onPlay() {
    this.setState({
      playing: true,
      chapterStarted: true,
    });
  }

  onPause() {
    this.setState({ playing: false });
  }

  onEnded() {
    this.setState({
      playing: false,
      chapterCompleted: true,
    });
  }

  onDuration(duration) {
    this.setState({ duration });
  }

  render() {
    let videoUrl = this.props.url;
    let start = 0;

    return (
      <div>
        <ReactPlayer
          url="https://s3-video-stream-delete.s3.amazonaws.com/SampleVideo_1280x720_1mb"
          onProgress={this.onProgress}
          onDuration={this.onDuration}
          onPlay={this.onPlay}
          onPause={this.onPause}
          onEnded={this.onEnded}
        />
      </div>
    );
  }
}
