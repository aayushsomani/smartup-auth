import React, { useEffect, Component } from "react";
import { observer, inject } from "mobx-react";

@inject("authStore")
@observer
class AuthCallBack extends Component {
  componentWillMount() {
    this.props.authStore.completeLogin();
  }
  render() {
    return <div>hello</div>;
  }
}

export default AuthCallBack;
