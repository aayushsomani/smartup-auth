import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App.jsx";
import * as serviceWorker from "./serviceWorker";
import { Provider } from "mobx-react";
import { AuthenticationStore } from "./stores/AuthenticationStore";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import AuthCallBack from "./components/AuthCallBack";

ReactDOM.render(
  <React.StrictMode>
    <Provider authStore={new AuthenticationStore()}>
      <Router>
        <Switch>
          <Route path="/auth-callback">
            <AuthCallBack />
          </Route>
          <Route path="/">
            <App />
          </Route>
        </Switch>
      </Router>
    </Provider>
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
